#ifndef ROOTH_UTILS
#define ROOTH_UTILS

#include <iostream>
#include <sstream>
#include <vector>
#include <limits>

#include <TROOT.h>
#include <TFile.h>
#include <TKey.h>
#include <TCollection.h>
#include <TSystem.h>
#include <TClass.h>
#include <TH1.h>
#include <TH2.h>
#include <TEfficiency.h>
#include <TProfile.h>
#include <TGraph.h>

using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::ostringstream;

namespace rooth_utils {

  class diff {
  public:
    diff();
    double m_val_absmax;
    double m_val_absav;
    double m_val_relmax;
    double m_val_relav;
    double m_val_sigmax;
    double m_val_sigav;
    double m_err_absmax;
    double m_err_absav;
    double m_err_relmax;
    double m_err_relav;   
  };
    
  void allobjecs_indir(TDirectory* const p_source, string p_dirup, vector<TKey*>& p_allkeys, vector<string>& p_alldirnames);
  bool is_nearly_equal(double p_a, double p_b, double p_prec=std::numeric_limits<double>::min());
  bool can_compare(TH1* const p_ref,TH1* const p_test);
  bool can_compare(TH2* const p_ref,TH2* const p_test);
  diff get_diff(TH1* const p_ref,TH1* const p_test,double p_prec=std::numeric_limits<double>::min());
  diff get_diff(TH2* const p_ref,TH2* const p_test,double p_prec=std::numeric_limits<double>::min());
  diff get_diff(TEfficiency* const p_ref,TEfficiency* const p_test,double p_prec=std::numeric_limits<double>::min());
  string get_type(TKey* const p_key);

  const string nss="rooth_utils";

}

#endif

