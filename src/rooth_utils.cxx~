#include <math.h>

#include "TStyle.h"
#include "TLatex.h"
#include "TLine.h"
#include "TMultiGraph.h"
#include "TLatex.h"

#include "rooth_utils.h"

// 0..4: silent,error,info,debug,verbose
# define PRINTLVL 3

rooth_utils::diff::diff() {
  m_val_absmax=-1.;
  m_val_absav=-1.;
  m_val_relmax=-1.;
  m_val_relav=-1.;
  m_val_sigmax=-1.;
  m_val_sigav=-1.;
  m_err_absmax=-1.;
  m_err_absav=-1.;
  m_err_relmax=-1.;
  m_err_relav=-1.;   
}

bool rooth_utils::is_nearly_equal(double p_a, double p_b, double p_prec) {

  double diff=fabs(p_a-p_b);
  return (diff<p_prec) ? true : false;  
}

bool rooth_utils::can_compare(TH2* const p_ref,TH2* const p_test) {
  
  string meth_name="can_compare";
  // check if valid comparison can be made
  if  (!p_ref) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: empty ref hist " << endl;
    }
    return(false);
  }
  if  (!p_test) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: empty test hist " << endl;
    }
    return(false);
  }
  unsigned int nbinsx_ref=p_ref->GetXaxis()->GetNbins();
  unsigned int nbinsx_test=p_test->GetXaxis()->GetNbins();
  unsigned int nbinsy_ref=p_ref->GetYaxis()->GetNbins();
  unsigned int nbinsy_test=p_test->GetYaxis()->GetNbins();  
  if (nbinsx_ref!=nbinsx_test && nbinsy_ref!=nbinsy_test) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: hists have different #bins " << endl;
    }
    return(false);
  }
  return(true);
}

bool rooth_utils::can_compare(TH1* const p_ref,TH1* const p_test) {
  
  string meth_name="can_compare";
  // check if valid comparison can be made
  if  (!p_ref) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: empty ref hist " << endl;
    }
    return(false);
  }
  if  (!p_test) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: empty test hist " << endl;
    }
    return(false);
  }
  unsigned int nbins_ref=p_ref->GetXaxis()->GetNbins();
  unsigned int nbins_test=p_test->GetXaxis()->GetNbins();
  if (nbins_ref!=nbins_test) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: hists have different #bins " << endl;
    }
    return(false);
  }
  return(true);
}

rooth_utils::diff rooth_utils::get_diff(TH1* const p_ref,TH1* const p_test,double p_prec) {

  // get difference between two TH1-s, ignore differences smaller than p_prec

  rooth_utils::diff ret_diff;
  
  if (!rooth_utils::can_compare(p_ref,p_test)) 
    return(ret_diff);

  // nbins + under + over-flow
  unsigned int n_allbins=p_ref->GetXaxis()->GetNbins()+2;
    
  for (unsigned int ite=0; ite<n_allbins; ++ite) {

    double bval_ref=p_ref->GetBinContent(ite);
    double bval_test=p_test->GetBinContent(ite);
    double be_ref=p_ref->GetBinError(ite);
    double be_test=p_test->GetBinError(ite);

    double bval_absdiff=fabs(bval_ref-bval_test);
    double _av=0.5*fabs(bval_ref+bval_test);
    double bval_reldiff= (is_nearly_equal(0.,_av,p_prec)) ? -1. :  bval_absdiff/_av;
    double _sig=sqrt(be_ref*be_ref+be_test*be_test);
    double bval_sigdiff= (is_nearly_equal(0.,_sig,p_prec)) ? -1. :  bval_absdiff/_sig;
    double be_absdiff=fabs(be_ref-be_test);
    double _eav=0.5*fabs(be_ref+be_test);
    double be_reldiff = (is_nearly_equal(0.,_eav,p_prec)) ? -1. :  be_absdiff/_eav;

    if (!is_nearly_equal(0.,bval_absdiff,p_prec) && bval_absdiff>0.) {
      if (bval_absdiff>ret_diff.m_val_absmax)
	ret_diff.m_val_absmax=bval_absdiff;
      if (ret_diff.m_val_absav<0.)
	ret_diff.m_val_absav=0.;
      ret_diff.m_val_absav+=bval_absdiff;
    }
    if (!is_nearly_equal(0.,bval_reldiff,p_prec) && bval_reldiff>0.) {
      if (bval_reldiff>ret_diff.m_val_relmax)
	ret_diff.m_val_relmax=bval_reldiff;
      if (ret_diff.m_val_relav<0.)
	ret_diff.m_val_relav=0.;      
      ret_diff.m_val_relav+=bval_reldiff;
    }
    if (!is_nearly_equal(0.,bval_sigdiff,p_prec) && bval_sigdiff>0.) {
      if (bval_sigdiff>ret_diff.m_val_sigmax)
	ret_diff.m_val_sigmax=bval_sigdiff;
      if (ret_diff.m_val_sigav<0.)
	ret_diff.m_val_sigav=0.;       
      ret_diff.m_val_sigav+=bval_sigdiff;
    }    
    if (!is_nearly_equal(0.,be_absdiff,p_prec) && be_absdiff>0.) {
      if (be_absdiff>ret_diff.m_err_absmax) {
	ret_diff.m_err_absmax=be_absdiff;
      }
      if (ret_diff.m_err_absav<0.)
	ret_diff.m_err_absav=0.;
      ret_diff.m_err_absav+=be_absdiff;
    }
    if (!is_nearly_equal(0.,be_reldiff,p_prec) && be_reldiff>0.) {
      if (be_reldiff>ret_diff.m_err_relmax)
	ret_diff.m_err_relmax=be_reldiff;
      if (ret_diff.m_err_relav<0.)
	ret_diff.m_err_relav=0.; 
      ret_diff.m_err_relav+=be_reldiff;
    }    
  }// end of loop over all bins

  // eval averages in case they were set: 
  if (ret_diff.m_val_absav>0.)
    ret_diff.m_val_absav*=1./n_allbins;
  if (ret_diff.m_val_relav>0.)
    ret_diff.m_val_relav*=1./n_allbins;  
  if (ret_diff.m_val_sigav>0.)
    ret_diff.m_val_sigav*=1./n_allbins;
  if (ret_diff.m_err_absav>0.)
    ret_diff.m_err_absav*=1./n_allbins;
  if (ret_diff.m_err_relav>0.)
    ret_diff.m_err_relav*=1./n_allbins;
  
  return(ret_diff);
}


rooth_utils::diff rooth_utils::get_diff(TH2* const p_ref,TH2* const p_test,double p_prec) {

  // get difference between two TH2-s, ignore differences smaller than p_prec

  rooth_utils::diff ret_diff;
  
  if (!rooth_utils::can_compare(p_ref,p_test)) 
    return(ret_diff);

  // nbins + under + over-flow
  unsigned int n_allbinsx=p_ref->GetXaxis()->GetNbins()+2;
  unsigned int n_allbinsy=p_ref->GetYaxis()->GetNbins()+2;
  
  for (unsigned int itex=0; itex<n_allbinsx; ++itex) {
    for (unsigned int itey=0; itey<n_allbinsy; ++itey) {
      double bval_ref=p_ref->GetBinContent(itex,itey);
      double bval_test=p_test->GetBinContent(itex,itey);
      double be_ref=p_ref->GetBinError(itex,itey);
      double be_test=p_test->GetBinError(itex,itey);

      double bval_absdiff=fabs(bval_ref-bval_test);
      double _av=0.5*fabs(bval_ref+bval_test);
      double bval_reldiff= (is_nearly_equal(0.,_av,p_prec)) ? -1. :  bval_absdiff/_av;
      double _sig=sqrt(be_ref*be_ref+be_test*be_test);
      double bval_sigdiff= (is_nearly_equal(0.,_sig,p_prec)) ? -1. :  bval_absdiff/_sig;
      double be_absdiff=fabs(be_ref-be_test);
      double _eav=0.5*fabs(be_ref+be_test);
      double be_reldiff = (is_nearly_equal(0.,_eav,p_prec)) ? -1. :  be_absdiff/_eav;

      if (!is_nearly_equal(0.,bval_absdiff,p_prec) && bval_absdiff>0.) {
	if (bval_absdiff>ret_diff.m_val_absmax)
	  ret_diff.m_val_absmax=bval_absdiff;
	if (ret_diff.m_val_absav<0.)
	ret_diff.m_val_absav=0.;
	ret_diff.m_val_absav+=bval_absdiff;
      }
      if (!is_nearly_equal(0.,bval_reldiff,p_prec) && bval_reldiff>0.) {
	if (bval_reldiff>ret_diff.m_val_relmax)
	  ret_diff.m_val_relmax=bval_reldiff;
	if (ret_diff.m_val_relav<0.)
	  ret_diff.m_val_relav=0.;      
	ret_diff.m_val_relav+=bval_reldiff;
      }
      if (!is_nearly_equal(0.,bval_sigdiff,p_prec) && bval_sigdiff>0.) {
	if (bval_sigdiff>ret_diff.m_val_sigmax)
	  ret_diff.m_val_sigmax=bval_sigdiff;
	if (ret_diff.m_val_sigav<0.)
	  ret_diff.m_val_sigav=0.;       
	ret_diff.m_val_sigav+=bval_sigdiff;
      }    
      if (!is_nearly_equal(0.,be_absdiff,p_prec) && be_absdiff>0.) {
	if (be_absdiff>ret_diff.m_err_absmax) {
	  ret_diff.m_err_absmax=be_absdiff;
	}
	if (ret_diff.m_err_absav<0.)
	ret_diff.m_err_absav=0.;
	ret_diff.m_err_absav+=be_absdiff;
      }
      if (!is_nearly_equal(0.,be_reldiff,p_prec) && be_reldiff>0.) {
	if (be_reldiff>ret_diff.m_err_relmax)
	  ret_diff.m_err_relmax=be_reldiff;
	if (ret_diff.m_err_relav<0.)
	  ret_diff.m_err_relav=0.; 
	ret_diff.m_err_relav+=be_reldiff;
      }
    }// end of loop over y-bins
  }// end of loop over x-bins

  // eval averages in case they were set: 
  if (ret_diff.m_val_absav>0.)
    ret_diff.m_val_absav*=1./(n_allbinsx*n_allbinsy);
  if (ret_diff.m_val_relav>0.)
    ret_diff.m_val_relav*=1./(n_allbinsx*n_allbinsy);  
  if (ret_diff.m_val_sigav>0.)
    ret_diff.m_val_sigav*=1./(n_allbinsx*n_allbinsy);
  if (ret_diff.m_err_absav>0.)
    ret_diff.m_err_absav*=1./(n_allbinsx*n_allbinsy);
  if (ret_diff.m_err_relav>0.)
    ret_diff.m_err_relav*=1./(n_allbinsx*n_allbinsy);
  
  return(ret_diff);
}


string rooth_utils::get_type(TKey* const p_key) {
  
  TClass *cl = gROOT->GetClass(p_key->GetClassName());
  string type="";
  if (cl->InheritsFrom(TDirectory::Class()))
    type="TDirectory";
  else if (cl->InheritsFrom(TProfile::Class()))
    type="TProfile";
  else if (cl->InheritsFrom(TH1F::Class()))
    type="TH1F";
  else if (cl->InheritsFrom(TH1D::Class()))
    type="TH1D";
  else if (cl->InheritsFrom(TH2F::Class()))
    type="TH2F";
  else if (cl->InheritsFrom(TH2D::Class()))
    type="TH2D";
  else if (cl->InheritsFrom(TEfficiency::Class()))
    type="TEfficiency";
  else if (cl->InheritsFrom(TGraph::Class()))
    type="TGraph";
  return(type);

}

rooth_utils::diff rooth_utils::get_diff(TEfficiency* const p_ref,TEfficiency* const p_test,double p_prec) {
  
  rooth_utils::diff ret_diff;
  // approximate diff with the diff between hist ratios
  if (!p_ref || !p_test)
    return(ret_diff);
  
  TH1* h_ref= p_ref->GetCopyPassedHisto();
  h_ref->Sumw2();
  h_ref->Divide(p_ref->GetCopyTotalHisto());

  TH1* h_test= p_test->GetCopyPassedHisto();
  h_test->Sumw2();
  h_test->Divide(p_test->GetCopyTotalHisto());
    
  if (!rooth_utils::can_compare(h_ref,h_test)) 
    return(ret_diff);
  
  ret_diff=rooth_utils::get_diff(h_ref,h_test,p_prec);

  return(ret_diff);
}

void rooth_utils::allobjecs_indir(TDirectory* const p_source, string p_dirup, vector<TKey*>& p_allkeys, vector<string>& p_alldirnames) {
  
  // recursively get list of all non-directory stuff in the directory
  vector<string> _allobjects;
  
  TIter nextkey(p_source->GetListOfKeys());
  TDirectory *savdir = gDirectory;
  TKey *key;
  
  while ((key=(TKey*)nextkey())) {
    TClass *cl = gROOT->GetClass(key->GetClassName());
    //cout << "looping for " << key->GetName() << endl;
    if (!cl){
      cout << "!cl continue " << endl;
      continue;
    }
    if (cl->InheritsFrom(TDirectory::Class())) {
      if (1<PRINTLVL)
	cout << nss << " found directory " << key->GetName() << endl;
      p_source->cd(key->GetName());
      TDirectory *subdir = gDirectory;
      string _oneup=subdir->GetName();
      string _p_dirup = (""!=p_dirup) ? p_dirup + "/" + _oneup : _oneup;
      allobjecs_indir(subdir,_p_dirup,p_allkeys,p_alldirnames);
    }
    else {
      p_allkeys.push_back(key);
      p_alldirnames.push_back(p_dirup+"/");
    }
    //cout << "cd-ing to savdir " << savdir->GetName() << std::endl;
    savdir->cd();
  }
  
  return;
}

vector<int> rooth_utils::get_colors(unsigned int p_size) {
  /*
kWhite  = 0,   kBlack  = 1,   kGray    = 920,  kRed    = 632,  kGreen  = 416,
kBlue   = 600, kYellow = 400, kMagenta = 616,  kCyan   = 432,  kOrange = 800,
kSpring = 820, kTeal   = 840, kAzure   =  860, kViolet = 880,  kPink   = 900
  */
  if (p_size <= 4) {
    // black, kRed+1,kBlue+1,kGreen+1
    return vector<int> {1,633,601,417};
  }
  else if (p_size<8) {
    // black, kOrange+1,kRed+1,kMagenta+2,kBlue+1,kBlue-7,kCyan+1,kGreen+1
    return vector<int> {1,801,633,618,601,597,433,417};
  }
  // all black
  vector<int> vdef (p_size,1);
  return vdef;
}

vector<int> rooth_utils::get_markers(unsigned int p_size) {
  if (p_size <= 4) {
    // dot, full circ, open circ, full square
    return vector<int> {1,20,24,21};
  }
  else if (p_size <= 8) {
    // dot, full circ, open circ, full square, open square, full triup, full tridown,open triup
    return vector<int> {1,20,24,21,25,22,23,26};
  }
  // full circ
  vector<int> vdef (p_size,20);
  return vdef;
}

void rooth_utils::print_label(vector<string> p_text, double p_x, double p_y) {
  double tsize=0.04;
  for (auto line : p_text) {
    TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);                         
    l.SetNDC();
    l.SetTextFont(42);
    l.SetTextSize(tsize);
    l.DrawLatex(p_x,p_y,line.c_str());
    p_y-=tsize*1.1;
  }
  return;
}

void rooth_utils::make_fig(vector<TGraph*> p_graphs, string p_figprefix,
			   vector<string> p_leg, vector<string> p_stamp) {
  
  if (p_graphs.empty() || !p_graphs[0]) {
    cout << nss << " make_fig empty graph " << endl;
    return;
  }
  
  double _cwidth = 800.;
  double _cheight = 600.; 
  TCanvas _c("_c","_c",_cwidth,_cheight);
  gStyle->SetOptStat(0);
  _c.cd();

  vector<int> cols=rooth_utils::get_colors(p_graphs.size());
  vector<int> marks=rooth_utils::get_markers(p_graphs.size()+1);
  unsigned int i=0;
  TMultiGraph* mg = new TMultiGraph();
  for (auto _git : p_graphs) {    
    _git->SetMarkerStyle(marks[i+1]);
    _git->SetMarkerSize(1.2);
    _git->SetMarkerColor(cols[i]);
    _git->SetLineColor(cols[i]);
    mg->Add(_git);
    //_git->DrawClone("sameAP");
    ++i;
  }

  mg->Draw("AP");
  string xtit=p_graphs[0]->GetXaxis()->GetTitle();
  string ytit=p_graphs[0]->GetYaxis()->GetTitle();
  mg->GetXaxis()->SetTitle(xtit.c_str());
  mg->GetYaxis()->SetTitle(ytit.c_str());  
  //gPad->Modified();

  TLegend* leg = rooth_utils::build_legend(p_graphs,p_leg);
  leg->Draw();
  
  rooth_utils::print_label(p_stamp);
  
  gPad->Update();

  ostringstream _fullname;
  _fullname << p_figprefix << "_" << p_graphs[0]->GetName() << ".png";
  _c.SaveAs(_fullname.str().c_str());
  
  return;
}
void rooth_utils::make_fig(vector<TH1*> p_hists, bool isprof, string p_figprefix,
			   vector<string> p_leg) {
  
  if (p_hists.empty() || !p_hists[0]) {
     cout << nss << " makefig empty hist " << endl;
     return;
  }

  // protect the original pointers from being overwritten
  vector<TH1*> hists;
  for (auto hist : p_hists)
    hists.push_back((TH1*)hist->Clone());
  p_hists.clear();
  
  bool DORAT=(hists.size()>1)?true:false;
  double _cwidth = 800.;
  double _cheight = (DORAT)?800.:600.;  
  Double_t px1, px2, py1, py2, p1y1;
  px1=0.; py1=0.; px2=1.; py2=0.95;
  p1y1=(DORAT)?0.4:py1;
  
  TCanvas _c("_c","_c",_cwidth,_cheight);
  gStyle->SetOptStat(0);
  _c.cd();
  
  TPad pad1("pad1","top pad",px1,p1y1,px2,py2);
  pad1.SetFillStyle(0);
  pad1.SetFrameFillStyle(0);
  pad1.Draw();
  
  TPad pad2("pad2","bottom pad",px1,py1,px2,p1y1);
  pad2.SetFillStyle(0);
  pad2.SetFrameFillStyle(0);
  pad2.Draw();
    
  pad1.cd();
  
  vector<int> cols=rooth_utils::get_colors(hists.size());
  vector<int> marks=rooth_utils::get_markers(hists.size());
  unsigned int i=0;
  for (auto _hit : hists) {    
    _hit->SetMarkerStyle(marks[i]);
    _hit->SetMarkerSize(1.2);
    _hit->SetMarkerColor(cols[i]);
    _hit->SetLineColor(cols[i]);
    _hit->DrawClone("samep");
    ++i;
  }
  hists[0]->DrawClone("samehist");

  TLegend* leg = rooth_utils::build_legend(hists,p_leg);
  leg->Draw();
  gPad->Update();
  
  if (DORAT){
    
    pad2.cd();

    TH1D *h0=NULL;
    if (isprof) {
      TProfile* p0=(TProfile*)hists[0];
      h0 = p0->ProjectionX();
      h0->Sumw2();
    }
    for (i=1; i<hists.size(); i++) {
      if (!isprof) {
	hists[i]->Sumw2();
	hists[i]->Divide(hists[0]);
	hists[i]->SetTitle("");
	hists[i]->GetYaxis()->SetTitle("");
	hists[i]->GetXaxis()->SetTitle("");
	hists[i]->DrawClone("samehistE");
      }
      else {
	// profile ratio to get meaningfull errors
	TProfile* ptmp = (TProfile*)hists[i];
	TH1D *htmp=ptmp->ProjectionX();
	htmp->SetMarkerStyle(ptmp->GetMarkerStyle());
	htmp->SetMarkerSize(ptmp->GetMarkerSize());
	htmp->SetMarkerColor(ptmp->GetMarkerColor());
	htmp->SetLineColor(ptmp->GetLineColor());
	htmp->Divide(h0);
	htmp->SetTitle("");
	htmp->GetYaxis()->SetTitle("");
	htmp->GetXaxis()->SetTitle("");
	htmp->DrawClone("samehistE");
      }
    }

    gPad->Update();
    TLine *line = new TLine(gPad->GetUxmin(),1.,gPad->GetUxmax(),1.0);
    line->SetLineStyle(1);
    line->SetLineColor(hists[0]->GetLineColor());
    line->SetLineWidth(2.);
    line->Draw("same");
    
  }

  ostringstream _fullname;
  _fullname << p_figprefix << "_" << hists[0]->GetName() << ".png";
  _c.SaveAs(_fullname.str().c_str());

  return;
}

void rooth_utils::make_fig(vector<TH2*> p_hists, string p_figprefix,
			   vector<string> p_fnames, double p_prec) {

  // ensure making figures does not modify the original pointers
  // cannot be enforced by constness, but likely good to do
  vector<TH2*> hists;
  for (auto hist : p_hists)
    hists.push_back((TH2*)hist->Clone());
  p_hists.clear();
  if (hists.empty() || !hists[0]) {
    cout << nss << " make_fig empty hist " << endl;
    return;
  }

  if(!hists.size()==p_fnames.size()) {
    cout << nss << " make_fig hists.size()==p_fnames.size() " << endl;
    return;
  }
   
  gStyle->SetOptStat(0);

  unsigned int ind=0;
  for (auto _hit : hists) {
    double _cwidth = 800.;
    double _cheight = 600.;
    TCanvas _c("_c","_c",_cwidth,_cheight); 
    _hit->Draw("COLZ");
    ostringstream _fullname;
    _fullname << p_figprefix << "_" << _hit->GetName() << "_" << p_fnames[ind] << ".png";
    _c.SaveAs(_fullname.str().c_str());
    ++ind;
  }

  int i=0;
  for (auto _hit : hists) {
    // draw rel diff in signifficance assuming uncorrelated hists
    if (0==i) {
      ++i;
      continue;
    }
    if (rooth_utils::can_compare(hists[0],_hit)) {
      double zmin,zmax;
      zmin=9.;
      zmax=-9.;
      // nbins + under + over-flow
      unsigned int n_allbinsx=hists[0]->GetXaxis()->GetNbins()+2;
      unsigned int n_allbinsy=hists[0]->GetYaxis()->GetNbins()+2;
      TH2* h_sigdiff= (TH2*)hists[0]->Clone();
      for (unsigned int itex=0; itex<n_allbinsx; ++itex) {
	for (unsigned int itey=0; itey<n_allbinsy; ++itey) {
	  double bval_ref=hists[0]->GetBinContent(itex,itey);
	  double bval_test=_hit->GetBinContent(itex,itey);
	  double be_ref=hists[0]->GetBinError(itex,itey);
	  double be_test=_hit->GetBinError(itex,itey);	  
	  double bval_absdiff=fabs(bval_ref-bval_test);
	  double _sig=sqrt(be_ref*be_ref+be_test*be_test);
	  double bval_sigdiff=0.;
	  if (is_nearly_equal(0.,bval_absdiff,p_prec)) {
	    bval_sigdiff=0.;
	  }
	  // sizablediff but negligible error ==> can't get diff [signifficance]
	  else if (is_nearly_equal(0.,_sig,p_prec*bval_absdiff)) {
	    bval_sigdiff=-9.;
	  }
	  else {
	    bval_sigdiff= bval_absdiff/_sig;
	  }
	  h_sigdiff->SetBinContent(itex,itey,bval_sigdiff);
	  if (bval_sigdiff<zmin)
	    zmin=bval_sigdiff;
	  if (bval_sigdiff>zmax)
	    zmax=bval_sigdiff;
	} // end of ybins looop
      }// end of xbins loop
      double _cwidth = 800.;
      double _cheight = 600.;
      TCanvas _c("_c","_c",_cwidth,_cheight); 
      h_sigdiff->Draw("COL");
      h_sigdiff->GetZaxis()->SetRangeUser(zmin,zmax);
      h_sigdiff->Draw("SAMECOLZ");
      ostringstream _fullname;
      _fullname << p_figprefix << "_" << hists[0]->GetName() << "_vs_" << _hit->GetName() << "_" << p_fnames[0] << "_vs_" << p_fnames[i] << ".png";
      _c.SaveAs(_fullname.str().c_str());
    }// end of can_compare  condition
    ++i;
  } // end of loop over hists
  return;
}

void rooth_utils::make_fig(vector<TEfficiency*> p_effs, string p_figprefix,
			   vector<string> p_leg) {
  
  if (p_effs.empty() || !p_effs[0]) {
    cout << nss << " makefig empty hist " << endl;
    return;
  }

  // protect the original pointers from being modified
  vector<TEfficiency*> effs;
  for (auto eff : p_effs)
    effs.push_back((TEfficiency*)eff->Clone());
  p_effs.clear();

  bool DORAT=(effs.size()>1)?true:false;
  double _cwidth = 800.;
  double _cheight = (DORAT)?800.:600.;  
  Double_t px1, px2, py1, py2, p1y1;
  px1=0.; py1=0.; px2=1.; py2=0.95;
  p1y1=(DORAT)?0.4:py1;
  
  TCanvas _c("_c","_c",_cwidth,_cheight);
  gStyle->SetOptStat(0);
  _c.cd();
  
  TPad pad1("pad1","top pad",px1,p1y1,px2,py2);
  pad1.SetFillStyle(0);
  pad1.SetFrameFillStyle(0);
  pad1.Draw();
  
  TPad pad2("pad2","bottom pad",px1,py1,px2,p1y1);
  pad2.SetFillStyle(0);
  pad2.SetFrameFillStyle(0);
  pad2.Draw();
    
  pad1.cd();
  
  vector<int> cols=rooth_utils::get_colors(effs.size());
  vector<int> marks=rooth_utils::get_markers(effs.size());
  unsigned int i=0;
  effs[0]->Draw();
  for (auto _hit : effs) {
    _hit->SetMarkerStyle(marks[i]);
    _hit->SetMarkerSize(1.2);
    _hit->SetMarkerColor(cols[i]);
    _hit->SetLineColor(cols[i]);
    _hit->Draw("same");
    ++i;
  }

  TLegend* leg = rooth_utils::build_legend(effs,p_leg);
  leg->Draw();

    
  if (DORAT){
    
    pad2.cd();

    TH1* h0t= effs[0]->GetCopyTotalHisto();
    TH1* h0p= effs[0]->GetCopyPassedHisto();
    h0t->Sumw2();
    h0p->Sumw2();
    h0p->Divide(h0t);
    for (i=1; i<effs.size(); i++) {
      TH1* ht= effs[i]->GetCopyTotalHisto();
      TH1* hp= effs[i]->GetCopyPassedHisto();
      ht->Sumw2();
      hp->Sumw2();
      hp->Divide(ht);
      hp->Divide(h0p);
      hp->SetMarkerStyle(effs[i]->GetMarkerStyle());
      hp->SetMarkerSize(effs[i]->GetMarkerSize());
      hp->SetMarkerColor(effs[i]->GetMarkerColor());
      hp->SetLineColor(effs[i]->GetLineColor());
      hp->SetTitle("");
      hp->GetYaxis()->SetTitle("");
      hp->GetXaxis()->SetTitle("");
      hp->DrawClone("samehistE");
    }

    gPad->Update();
    
    TLine *line = new TLine(gPad->GetUxmin(),1.,gPad->GetUxmax(),1.0);
    line->SetLineStyle(1);
    line->SetLineColor(effs[0]->GetLineColor());
    line->SetLineWidth(2.);
    line->Draw("same");
    
  }

  
  ostringstream _fullname;
  _fullname << p_figprefix << "_" << effs[0]->GetName() << ".png";
  _c.SaveAs(_fullname.str().c_str());

  return;

}



TLegend* rooth_utils::build_legend(vector<TGraph*> p_graphs,vector<string> p_leg) {
  double lx1,lx2,ly1,ly2;
  lx1=0.70; ly2=0.88; ly1=ly2-0.07*p_leg.size(); lx2=0.90;
  TLegend* leg= new TLegend(lx1,ly1,lx2,ly2);
  if (p_leg.size()==p_graphs.size()) {    
    leg->SetFillColor(0);
    leg->SetShadowColor(0);
    leg->SetBorderSize(0);
    for (unsigned i=0; i<p_leg.size(); i++) 
      leg->AddEntry(p_graphs[i],p_leg[i].c_str(),"p");
  }
  return leg; 
}

TLegend* rooth_utils::build_legend(vector<TEfficiency*> p_effs,vector<string> p_leg) {
  double lx1,lx2,ly1,ly2;
  lx1=0.70; ly2=0.88; ly1=ly2-0.07*p_leg.size(); lx2=0.90;
  TLegend* leg= new TLegend(lx1,ly1,lx2,ly2);
  if (p_leg.size()==p_effs.size()) {    
    leg->SetFillColor(0);
    leg->SetShadowColor(0);
    leg->SetBorderSize(0);
    for (unsigned i=0; i<p_leg.size(); i++) 
      leg->AddEntry(p_effs[i],p_leg[i].c_str(),"pl");
  }
  return leg; 
}

TLegend* rooth_utils::build_legend(vector<TH1*> p_hists,vector<string> p_leg) {
  double lx1,lx2,ly1,ly2;
  lx1=0.70; ly2=0.88; ly1=ly2-0.07*p_leg.size(); lx2=0.90;
  TLegend* leg= new TLegend(lx1,ly1,lx2,ly2);
  if (p_leg.size()==p_hists.size()) {    
    leg->SetFillColor(0);
    leg->SetShadowColor(0);
    leg->SetBorderSize(0);
    for (unsigned i=0; i<p_leg.size(); i++) 
      leg->AddEntry(p_hists[i],p_leg[i].c_str(),"pl");
  }
  return leg; 
}
