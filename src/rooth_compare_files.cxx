#include <algorithm>
#include "rooth_compare_files.h"

# define PRINTLVL 1

int main(){

  vector<string> fnames {"ref","test"};
  
  vector<TFile*> tFiles;
  for (auto tf : fnames) 
    tFiles.push_back(new TFile((tf+".root").c_str(),"READ"));

  TFile ref_tFile((fnames[0]+".root").c_str(),"READ"); 
  TDirectory *topdir = gDirectory;  
  vector<TKey*> _allkeys;
  vector<string> _alldirnames;
  rooth_utils::allobjecs_indir(topdir,"",_allkeys,_alldirnames);
    
  int _ikey=0;
  for (auto _it : _allkeys) {

    string type=rooth_utils::get_type(_it);
    if (""==type)
      type="NotExpected";
    string name=_it->GetName();
    string fullname=_alldirnames[_ikey]+name;

    vector<TH1*> hists; 
    vector<TH2*> hists2;
    vector<TProfile*> profs;
    vector<TEfficiency*> effs;

   // any special treatment for histograms?
    if ("TH1F"==type || "TH1D"==type) {
      for (auto tf : tFiles) 
	hists.push_back((TH1*) tf->Get(fullname.c_str()));
    }
    else if ("TProfile"==type) {
      for (auto tf : tFiles) {
	profs.push_back((TProfile*) tf->Get(fullname.c_str()));
	hists.push_back((TH1*) tf->Get(fullname.c_str()));
      }
    }
    else if ("TEfficiency"==type) {
      for (auto tf : tFiles)
	effs.push_back((TEfficiency*) tf->Get(fullname.c_str()));
    }
    else if ("TH2F"==type || "TH2D"==type) { 
      for (auto tf : tFiles)
	hists2.push_back((TH2*) tf->Get(fullname.c_str()));
    }
    else {
      if (1<PRINTLVL)
	cout << __FILE__ " loop over keys: no treatment for " << type << " : " << fullname  << endl;
    }
  
    //----------- numerical comparisons ----------
    double prec=1e-4;
    if ("TH1F"==type || "TH1D"==type || "TProfile"==type) {
      int ihist=0;
      for (auto hist : hists) {
	if (0==ihist) {
	  ++ihist;
	  continue;
	}
	rooth_utils::diff diff=rooth_utils::get_diff(hists[0],hist,prec);
	bool is_val_ok = (diff.m_val_relmax>0.) ? 
	  rooth_utils::is_nearly_equal(0.,diff.m_val_relmax,prec) : true;
	bool is_err_ok = (diff.m_err_relmax>0.) ? 
	  rooth_utils::is_nearly_equal(0.,diff.m_err_relmax,prec) : true;	
	cout << type << ":" << fullname << " " << fnames[ihist] 
	     << " diff.m_val_relmax: " << diff.m_val_relmax << " diff.m_err_relmax: " << diff.m_err_relmax
	     << " is_ok: " << (is_val_ok && is_err_ok) << endl;
	++ihist;
      }// end of loop over hists 
    }// end of ("TH1F"==type || "TH1D"==type)
    else if ("TEfficiency"==type) {
      int ieff=0;
      for (auto eff : effs) {
	if (0==ieff) {
	  ++ieff;
	  continue;
	}
	rooth_utils::diff diff=rooth_utils::get_diff(effs[0],eff,prec);
	bool is_val_ok = (diff.m_val_relmax>0.) ? 
	  rooth_utils::is_nearly_equal(0.,diff.m_val_relmax,prec) : true;
	bool is_err_ok = (diff.m_err_relmax>0.) ? 
	  rooth_utils::is_nearly_equal(0.,diff.m_err_relmax,prec) : true;	
	cout << type << ":" << fullname << " " << fnames[ieff] 
	     << " diff.m_val_relmax: " << diff.m_val_relmax << " diff.m_err_relmax: " << diff.m_err_relmax
	     << " is_ok: " << (is_val_ok && is_err_ok) << endl;
	++ieff;
      }// end of loop over effs 
    }// end of ("TEfficiency")
    else if ("TH2F"==type || "TH2D"==type) {
      int ih2=0;
      for (auto h2 : hists2) {
	if (0==ih2) {
	  ++ih2;
	  continue;
	}
	rooth_utils::diff diff=rooth_utils::get_diff(hists2[0],h2,prec);
	bool is_val_ok = (diff.m_val_relmax>0.) ? 
	  rooth_utils::is_nearly_equal(0.,diff.m_val_relmax,prec) : true;
	bool is_err_ok = (diff.m_err_relmax>0.) ? 
	  rooth_utils::is_nearly_equal(0.,diff.m_err_relmax,prec) : true;	
	cout << type << ":" << fullname << " " << fnames[ih2] 
	     << " diff.m_val_relmax: " << diff.m_val_relmax << " diff.m_err_relmax: " << diff.m_err_relmax
	     << " is_ok: " << (is_val_ok && is_err_ok) << endl;
	++ih2;
      }// end of loop over hists2 
    }
    else {
      cout << "numerical comparison not supported for " << type << " ; " << fullname << endl;
    }
    //----------- end of numerical comparisons ----------
    
  ++_ikey;
  }// end of loop over all_keys
  
  return 0;
}

//////////////

  


