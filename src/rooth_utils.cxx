#include <math.h>

#include "TStyle.h"
#include "TLatex.h"
#include "TLine.h"
#include "TMultiGraph.h"
#include "TLatex.h"

#include "rooth_utils.h"

// 0..4: silent,error,info,debug,verbose
# define PRINTLVL 2

rooth_utils::diff::diff() {
  m_val_absmax=-1.;
  m_val_absav=-1.;
  m_val_relmax=-1.;
  m_val_relav=-1.;
  m_val_sigmax=-1.;
  m_val_sigav=-1.;
  m_err_absmax=-1.;
  m_err_absav=-1.;
  m_err_relmax=-1.;
  m_err_relav=-1.;   
}

bool rooth_utils::is_nearly_equal(double p_a, double p_b, double p_prec) {

  double diff=fabs(p_a-p_b);
  return (diff<p_prec) ? true : false;  
}

bool rooth_utils::can_compare(TH2* const p_ref,TH2* const p_test) {
  
  string meth_name="can_compare";
  // check if valid comparison can be made
  if  (!p_ref) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: empty ref hist " << endl;
    }
    return(false);
  }
  if  (!p_test) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: empty test hist " << endl;
    }
    return(false);
  }
  unsigned int nbinsx_ref=p_ref->GetXaxis()->GetNbins();
  unsigned int nbinsx_test=p_test->GetXaxis()->GetNbins();
  unsigned int nbinsy_ref=p_ref->GetYaxis()->GetNbins();
  unsigned int nbinsy_test=p_test->GetYaxis()->GetNbins();  
  if (nbinsx_ref!=nbinsx_test && nbinsy_ref!=nbinsy_test) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: hists have different #bins " << endl;
    }
    return(false);
  }
  return(true);
}

bool rooth_utils::can_compare(TH1* const p_ref,TH1* const p_test) {
  
  string meth_name="can_compare";
  // check if valid comparison can be made
  if  (!p_ref) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: empty ref hist " << endl;
    }
    return(false);
  }
  if  (!p_test) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: empty test hist " << endl;
    }
    return(false);
  }
  unsigned int nbins_ref=p_ref->GetXaxis()->GetNbins();
  unsigned int nbins_test=p_test->GetXaxis()->GetNbins();
  if (nbins_ref!=nbins_test) {
    if (2<PRINTLVL) {
      cout << nss<<"::"<<meth_name<<": "<<"can_compare: hists have different #bins " << endl;
    }
    return(false);
  }
  return(true);
}

rooth_utils::diff rooth_utils::get_diff(TH1* const p_ref,TH1* const p_test,double p_prec) {

  // get difference between two TH1-s, ignore differences smaller than p_prec

  rooth_utils::diff ret_diff;
  
  if (!rooth_utils::can_compare(p_ref,p_test)) 
    return(ret_diff);

  // nbins + under + over-flow
  unsigned int n_allbins=p_ref->GetXaxis()->GetNbins()+2;
    
  for (unsigned int ite=0; ite<n_allbins; ++ite) {

    double bval_ref=p_ref->GetBinContent(ite);
    double bval_test=p_test->GetBinContent(ite);
    double be_ref=p_ref->GetBinError(ite);
    double be_test=p_test->GetBinError(ite);

    double bval_absdiff=fabs(bval_ref-bval_test);
    double _av=0.5*fabs(bval_ref+bval_test);
    double bval_reldiff= (is_nearly_equal(0.,_av,p_prec)) ? -1. :  bval_absdiff/_av;
    double _sig=sqrt(be_ref*be_ref+be_test*be_test);
    double bval_sigdiff= (is_nearly_equal(0.,_sig,p_prec)) ? -1. :  bval_absdiff/_sig;
    double be_absdiff=fabs(be_ref-be_test);
    double _eav=0.5*fabs(be_ref+be_test);
    double be_reldiff = (is_nearly_equal(0.,_eav,p_prec)) ? -1. :  be_absdiff/_eav;

    if (!is_nearly_equal(0.,bval_absdiff,p_prec) && bval_absdiff>0.) {
      if (bval_absdiff>ret_diff.m_val_absmax)
	ret_diff.m_val_absmax=bval_absdiff;
      if (ret_diff.m_val_absav<0.)
	ret_diff.m_val_absav=0.;
      ret_diff.m_val_absav+=bval_absdiff;
    }
    if (!is_nearly_equal(0.,bval_reldiff,p_prec) && bval_reldiff>0.) {
      if (bval_reldiff>ret_diff.m_val_relmax)
	ret_diff.m_val_relmax=bval_reldiff;
      if (ret_diff.m_val_relav<0.)
	ret_diff.m_val_relav=0.;      
      ret_diff.m_val_relav+=bval_reldiff;
    }
    if (!is_nearly_equal(0.,bval_sigdiff,p_prec) && bval_sigdiff>0.) {
      if (bval_sigdiff>ret_diff.m_val_sigmax)
	ret_diff.m_val_sigmax=bval_sigdiff;
      if (ret_diff.m_val_sigav<0.)
	ret_diff.m_val_sigav=0.;       
      ret_diff.m_val_sigav+=bval_sigdiff;
    }    
    if (!is_nearly_equal(0.,be_absdiff,p_prec) && be_absdiff>0.) {
      if (be_absdiff>ret_diff.m_err_absmax) {
	ret_diff.m_err_absmax=be_absdiff;
      }
      if (ret_diff.m_err_absav<0.)
	ret_diff.m_err_absav=0.;
      ret_diff.m_err_absav+=be_absdiff;
    }
    if (!is_nearly_equal(0.,be_reldiff,p_prec) && be_reldiff>0.) {
      if (be_reldiff>ret_diff.m_err_relmax)
	ret_diff.m_err_relmax=be_reldiff;
      if (ret_diff.m_err_relav<0.)
	ret_diff.m_err_relav=0.; 
      ret_diff.m_err_relav+=be_reldiff;
    }    
  }// end of loop over all bins

  // eval averages in case they were set: 
  if (ret_diff.m_val_absav>0.)
    ret_diff.m_val_absav*=1./n_allbins;
  if (ret_diff.m_val_relav>0.)
    ret_diff.m_val_relav*=1./n_allbins;  
  if (ret_diff.m_val_sigav>0.)
    ret_diff.m_val_sigav*=1./n_allbins;
  if (ret_diff.m_err_absav>0.)
    ret_diff.m_err_absav*=1./n_allbins;
  if (ret_diff.m_err_relav>0.)
    ret_diff.m_err_relav*=1./n_allbins;
  
  return(ret_diff);
}


rooth_utils::diff rooth_utils::get_diff(TH2* const p_ref,TH2* const p_test,double p_prec) {

  // get difference between two TH2-s, ignore differences smaller than p_prec

  rooth_utils::diff ret_diff;
  
  if (!rooth_utils::can_compare(p_ref,p_test)) 
    return(ret_diff);

  // nbins + under + over-flow
  unsigned int n_allbinsx=p_ref->GetXaxis()->GetNbins()+2;
  unsigned int n_allbinsy=p_ref->GetYaxis()->GetNbins()+2;
  
  for (unsigned int itex=0; itex<n_allbinsx; ++itex) {
    for (unsigned int itey=0; itey<n_allbinsy; ++itey) {
      double bval_ref=p_ref->GetBinContent(itex,itey);
      double bval_test=p_test->GetBinContent(itex,itey);
      double be_ref=p_ref->GetBinError(itex,itey);
      double be_test=p_test->GetBinError(itex,itey);

      double bval_absdiff=fabs(bval_ref-bval_test);
      double _av=0.5*fabs(bval_ref+bval_test);
      double bval_reldiff= (is_nearly_equal(0.,_av,p_prec)) ? -1. :  bval_absdiff/_av;
      double _sig=sqrt(be_ref*be_ref+be_test*be_test);
      double bval_sigdiff= (is_nearly_equal(0.,_sig,p_prec)) ? -1. :  bval_absdiff/_sig;
      double be_absdiff=fabs(be_ref-be_test);
      double _eav=0.5*fabs(be_ref+be_test);
      double be_reldiff = (is_nearly_equal(0.,_eav,p_prec)) ? -1. :  be_absdiff/_eav;

      if (!is_nearly_equal(0.,bval_absdiff,p_prec) && bval_absdiff>0.) {
	if (bval_absdiff>ret_diff.m_val_absmax)
	  ret_diff.m_val_absmax=bval_absdiff;
	if (ret_diff.m_val_absav<0.)
	ret_diff.m_val_absav=0.;
	ret_diff.m_val_absav+=bval_absdiff;
      }
      if (!is_nearly_equal(0.,bval_reldiff,p_prec) && bval_reldiff>0.) {
	if (bval_reldiff>ret_diff.m_val_relmax)
	  ret_diff.m_val_relmax=bval_reldiff;
	if (ret_diff.m_val_relav<0.)
	  ret_diff.m_val_relav=0.;      
	ret_diff.m_val_relav+=bval_reldiff;
      }
      if (!is_nearly_equal(0.,bval_sigdiff,p_prec) && bval_sigdiff>0.) {
	if (bval_sigdiff>ret_diff.m_val_sigmax)
	  ret_diff.m_val_sigmax=bval_sigdiff;
	if (ret_diff.m_val_sigav<0.)
	  ret_diff.m_val_sigav=0.;       
	ret_diff.m_val_sigav+=bval_sigdiff;
      }    
      if (!is_nearly_equal(0.,be_absdiff,p_prec) && be_absdiff>0.) {
	if (be_absdiff>ret_diff.m_err_absmax) {
	  ret_diff.m_err_absmax=be_absdiff;
	}
	if (ret_diff.m_err_absav<0.)
	ret_diff.m_err_absav=0.;
	ret_diff.m_err_absav+=be_absdiff;
      }
      if (!is_nearly_equal(0.,be_reldiff,p_prec) && be_reldiff>0.) {
	if (be_reldiff>ret_diff.m_err_relmax)
	  ret_diff.m_err_relmax=be_reldiff;
	if (ret_diff.m_err_relav<0.)
	  ret_diff.m_err_relav=0.; 
	ret_diff.m_err_relav+=be_reldiff;
      }
    }// end of loop over y-bins
  }// end of loop over x-bins

  // eval averages in case they were set: 
  if (ret_diff.m_val_absav>0.)
    ret_diff.m_val_absav*=1./(n_allbinsx*n_allbinsy);
  if (ret_diff.m_val_relav>0.)
    ret_diff.m_val_relav*=1./(n_allbinsx*n_allbinsy);  
  if (ret_diff.m_val_sigav>0.)
    ret_diff.m_val_sigav*=1./(n_allbinsx*n_allbinsy);
  if (ret_diff.m_err_absav>0.)
    ret_diff.m_err_absav*=1./(n_allbinsx*n_allbinsy);
  if (ret_diff.m_err_relav>0.)
    ret_diff.m_err_relav*=1./(n_allbinsx*n_allbinsy);
  
  return(ret_diff);
}


string rooth_utils::get_type(TKey* const p_key) {
  
  TClass *cl = gROOT->GetClass(p_key->GetClassName());
  string type="";
  if (cl->InheritsFrom(TDirectory::Class()))
    type="TDirectory";
  else if (cl->InheritsFrom(TProfile::Class()))
    type="TProfile";
  else if (cl->InheritsFrom(TH1F::Class()))
    type="TH1F";
  else if (cl->InheritsFrom(TH1D::Class()))
    type="TH1D";
  else if (cl->InheritsFrom(TH2F::Class()))
    type="TH2F";
  else if (cl->InheritsFrom(TH2D::Class()))
    type="TH2D";
  else if (cl->InheritsFrom(TEfficiency::Class()))
    type="TEfficiency";
  else if (cl->InheritsFrom(TGraph::Class()))
    type="TGraph";
  return(type);

}

rooth_utils::diff rooth_utils::get_diff(TEfficiency* const p_ref,TEfficiency* const p_test,double p_prec) {
  
  rooth_utils::diff ret_diff;
  // approximate diff with the diff between hist ratios
  if (!p_ref || !p_test)
    return(ret_diff);
  
  TH1* h_ref= p_ref->GetCopyPassedHisto();
  h_ref->Sumw2();
  h_ref->Divide(p_ref->GetCopyTotalHisto());

  TH1* h_test= p_test->GetCopyPassedHisto();
  h_test->Sumw2();
  h_test->Divide(p_test->GetCopyTotalHisto());
    
  if (!rooth_utils::can_compare(h_ref,h_test)) 
    return(ret_diff);
  
  ret_diff=rooth_utils::get_diff(h_ref,h_test,p_prec);

  return(ret_diff);
}

void rooth_utils::allobjecs_indir(TDirectory* const p_source, string p_dirup, vector<TKey*>& p_allkeys, vector<string>& p_alldirnames) {
  
  // recursively get list of all non-directory stuff in the directory
  vector<string> _allobjects;
  
  TIter nextkey(p_source->GetListOfKeys());
  TDirectory *savdir = gDirectory;
  TKey *key;
  
  while ((key=(TKey*)nextkey())) {
    TClass *cl = gROOT->GetClass(key->GetClassName());
    //cout << "looping for " << key->GetName() << endl;
    if (!cl){
      cout << "!cl continue " << endl;
      continue;
    }
    if (cl->InheritsFrom(TDirectory::Class())) {
      if (1<PRINTLVL)
	cout << nss << " found directory " << key->GetName() << endl;
      p_source->cd(key->GetName());
      TDirectory *subdir = gDirectory;
      string _oneup=subdir->GetName();
      string _p_dirup = (""!=p_dirup) ? p_dirup + "/" + _oneup : _oneup;
      allobjecs_indir(subdir,_p_dirup,p_allkeys,p_alldirnames);
    }
    else {
      p_allkeys.push_back(key);
      p_alldirnames.push_back(p_dirup+"/");
    }
    //cout << "cd-ing to savdir " << savdir->GetName() << std::endl;
    savdir->cd();
  }
  
  return;
}

