CXX     = g++
LD      = g++

OBJ_DIR = obj
BIN_DIR = bin
SRC_DIR = src
INC_DIR = include

ROOTCINT     = rootcint
ROOTCONFIG   = root-config
ROOTCXXFLAGS = $(shell $(ROOTCONFIG) --cflags)
ROOTLIBS     = $(shell $(ROOTCONFIG) --libs)

LIBS = $(ROOTLIBS)
LIBS_EXTRA = $(ROOTLIBS)
INC = -I$(INC_DIR)

DEBUG        = false
ifeq ($(DEBUG),true)
	CXXFLAGS     = -O0 -Wall -ggdb -fPIC $(INC) $(ROOTCXXFLAGS)
	LDFLAGS      = -O0 -Wall -ggdb $(INC) $(ROOTCXXFLAGS)
else
	CXXFLAGS     = -O2 -Wall -fPIC $(INC) $(ROOTCXXFLAGS)
	LDFLAGS      = -O2 -Wall $(INC) $(ROOTCXXFLAGS)
endif

# generic rules

$(SRC_DIR)/%.cxx : $(INC_DIR)/%.h

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cxx
	$(CXX) $(CXXFLAGS) -c $^ -o $@

# file targets
OBJECT_ROOTH_COMPARE_FILES = $(OBJ_DIR)/rooth_compare_files.o
OBJECT_ROOTH_UTILS = $(OBJ_DIR)/rooth_utils.o

# main targets
MAIN_ROOTH_COMPARE_FILES = $(BIN_DIR)/rooth_compare_files

# rules
all: $(MAIN_ROOTH_COMPARE_FILES)
rooth_compare_files: $(MAIN_ROOTH_COMPARE_FILES)

# deps		
$(MAIN_ROOTH_COMPARE_FILES): $(OBJECT_ROOTH_UTILS) $(OBJECT_ROOTH_COMPARE_FILES) 
	$(CXX) $(CXXFLAGS) -o $@ -g $^ $(LIBS)

clean:
	rm -f $(OBJ_DIR)/*.o $(BIN_DIR)/[^C]* 
