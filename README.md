# Description
- Tests if objects in two root files are equivalent.
- Objects tested: TH2F,TH2D,TH1F,TH1D,TProfile,TEfficiency
- Test: loops over all object bins and tests whether
relative difference in all values and
relative difference of all errors < rounding precision?
Default rounding precision 1e-4. To change it,
look for this line in the rooth_compare_files.cxx

double prec=1e-4;

# Fetch: 
> git clone https://:@gitlab.cern.ch:8443/lmijovic/root_helpers_shared.git

# Prerequisites: 
Should run anywhere with modern gcc and root.Tested on lxplus.  

# Usage: 
setup as:
> asetup AtlasProduction,20.20.8.9,here  
> mkdir bin obj  

compile as:  
> make  

say you want to check if rootfile1.root and rootfile2.root have same contents.set them to conventional names: 
> ln -s rootfile1.root test.root  
> ln -s rootfile2.root ref.root

run comparison as: 
> ./bin/rooth_compare_files > out.out

The out file will contain lines in format:
- histogram type:histogramname {maximum |difference in bin value|} {maximum |difference in bin error|} is_ok: {0/1}
- is_ok: 0 means there is non-zero difference between the histograms (larger than the rounding precision)
- is_ok: 1 means histograms are identical (within rounding precision)

# Example outputs:
Identical histograms (is_ok: 1):  
> TH1F:IDPerformanceMon/Tracks/SelectedGoodTracks/ntrack test diff.m_val_relmax: -1 diff.m_err_relmax: -1 is_ok: 1

Here maximum |difference in bin value| = -1 and maximum |difference in bin error| = -1 are the default values

Histograms with a difference (is_ok: 0) : 
> TH1F:IDPerformanceMon/Tracks/SelectedGoodTracks/ntrack test diff.m_val_relmax: 2 diff.m_err_relmax: 2 is_ok: 0  

Here, I have up to 200% relative difference in for histogram values (diff.m_val_relmax: 2) and for histogram errors (diff.m_err_relmax: 2).

To check how many histograms differ, I could eg count them from the CL as:
> grep -c 'is_ok: 0' out.out  
> 5

Here, I was running a comparison in which five histograms differed. If all histograms are identical, the count will return 0.

